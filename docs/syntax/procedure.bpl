procedure sumFromOneToN(n: int) returns (sum: int)
requires n > 1
ensures sum >= n
{
    sum := 0
}