# Syntax by example
## types
https://boogie-docs.readthedocs.io/en/latest/LangRef.html#types

## function
A function has input arguments, one output and it's body contains an expression:

```boogie
function square(x:int): int {
    x*x
}
```

The body is optional and can be replaced with a `;`.

### calling a function
A function may call a function but not a procedure:
```boogie
function square(x:int): int {
    x*x
}
function doubleSquare(x:int):int {
    square(square(x))
}
```

### axioms: reasoning about functions
Instead of writing out a function body, the functions behavior can also be specified indirectly by using an axiom (see [axioms](#axioms)):

```boogie
function square(x:int): int {
    x*x
}

function square2(x:int): int;
axiom (forall i:int :: square2(i) == i*i);
// a simpler examle without a quantifier would be:
axiom square2(3) == 9;
```


### if-then-else-expression
There is a variant of if-else as an expression which can be used in functions:
```boogie
function abs(x:int): int {
    if x<0 then -x else x
}
```


## axioms
Axioms 

## procedure
A procedure contains imperative code. It can have multiple arguments and multiple return values. Return values act as normal variables and can be assigned.

```boogie
procedure square(x: int) returns (sq: int)
{
    sq := x*x;
}
```

### Pre- and Postconditions
Procedures can have pre and post conditions:
```boogie
procedure distance(x: int, y: int) returns (distance: int)
requires x >= y;
ensures distance >= 0;
{
    distance := x-y;
}
```

### Calling procedures

### control flow

### if-else
`if` exists both as a statement and as an expression.


<!-- ### asserts
Asserts can be used to test behavior. They can contain boolean expressions.
```boogie
assert 3*3 == 9;
``` -->
